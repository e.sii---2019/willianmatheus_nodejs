# Node.js

## Instalação
A instalação do Node.js é extremamente simples graças ao fato de o V8 JavaScript Engine ser completamente multi-plataforma, tudo que você precisa fazer é visitar a página oficial do Node.js, clicar na opção que melhor se encaixa a sua maquina e seguir as instruções.
Após a instalação, basta executar o seguinte comando no seu terminal para verificar se foi instalado corretamente:
```
$ node -v
> v0.10.26
```

deve retornar a versão do node que foi instalada, como por exemplo v0.10.26.

## Baixando o Projeto

Abra o terminal do Git Bash.
Navegue até o diretorio aonde deseja baixar essa aplicação. Exemplo:
```
$ cd desktop
$ cd Nova Pasta
```
Após encontrar a pasta desejada digite o comando no terminal:
```
git clone https://gitlab.com/williancm/willianmatheus_nodejs.git
```
Pronto, você já tem o projeto na sua maquina.

## Instalando as dependencias do Projeto
Abra o prompt de Comando, navegue até a pasta do projeto.
Após isso digite o comando:
```
npm install
```
Isso faz com que as dependencias necessárias para esse projeto sejam baixadas. Observa-se que na pasta do aplicativo teremos um novo diretorio chamado "node_modules", que é a pasta que guarda as dependencias do projeto.

## Criando o banco de Dados para a Aplicação
Na raiz do projeto, encontra-se um arquivo chamado 'script-1.0.sql', este arquivo vai criar a nossa tabela de times e estados.
Como executar esse arquivo:
Tecle a tecla Windowns e digite 'mysql', e abra a opção de 'Command Line Client'
Com o Terminal do MySQL aberto, digite comando 'source' seguido do caminho do arquivo.
```
source C:\~\Desktop\Nova pasta\willianmatheus_nodejs\scrpt-1.0.sql
```
O '~' entre a pasta C e a Desktop deve ser preenchido com o caminho que o diretorio se encontra.

## Rodando o projeto
Com o prompt de Comando aberto no diretorio da aplicação rode o seguinte comando:
```
node index.js
```
Isso faz com que o diretório comece a rodar em um porta determinada no arquivo index.js.
No seu navegador, informe a url localhost:5000.
Você poderá ver a pagina rodando.

## Video no Youtube
Passo a passo de como montar um esboço inicial desse projeto do zero
https://www.youtube.com/watch?v=7xcM5w9aS3c&feature=youtu.be





