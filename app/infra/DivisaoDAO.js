module.exports = function(){

	this.serieA = function(connection, callback){
		connection.query('SELECT times.id AS id, times.nome AS times, estados.nome AS estados FROM times  JOIN estados ON times.id_estado = estados.id WHERE id_divisao = 1', callback);
	}

	this.serieB = function(connection, callback){
		connection.query('SELECT times.id AS id, times.nome AS times, estados.nome AS estados FROM times  JOIN estados ON times.id_estado = estados.id WHERE id_divisao = 2', callback);
	}

	return this;
}