module.exports = function(app){

	app.get('/estados', function(req, res){
		var connection = app.infra.connectionFactory();
		var estadosList = app.infra.EstadosDAO;

		estadosList.lista(connection, function(err, resposta) {
			res.render('estado/lista', {list: resposta});
		});

		connection.end();

	});

}